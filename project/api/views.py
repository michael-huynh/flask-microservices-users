from flask import Blueprint, jsonify, request, make_response, render_template
from sqlalchemy import exc
from project.api.models import User
from project import db


users_blueprint = Blueprint('users', __name__, template_folder='./templates')


@users_blueprint.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        user_name = request.form['user_name']
        email = request.form['email']
        db.session.add(User(user_name=user_name, email=email))
        db.session.commit()
    users = User.query.order_by(User.created_at.desc()).all()
    return render_template('index.html', users=users)


@users_blueprint.route('/ping', methods=['GET'])
def ping_pong():
    return jsonify({
        'status': 'success',
        'message': 'pong!'
    })


@users_blueprint.route('/users', methods=['POST'])
def add_user():
    """Add a new user into database."""
    post_data = request.get_json()
    if not post_data:
        response_object = dict(
            status='fail',
            message=f'Invalid payload.'
        )
        return make_response(jsonify(response_object)), 400
    user_name = post_data.get('user_name')
    email = post_data.get('email')
    try:
        user = User.query.filter_by(email=email).first()
        if not user:
            db.session.add(User(user_name=user_name, email=email))
            db.session.commit()
            response_object = dict(
                status='success',
                message=f'{email} was added!'
            )
            return make_response(jsonify(response_object)), 201
        else:
            response_object = dict(
                status='fail',
                message=f'Sorry. That email already exists.'
            )
            return make_response(jsonify(response_object)), 400
    except exc.IntegrityError as e:
        db.session.rollback()
        response_object = dict(
            status='fail',
            message=f'Invalid payload.'
        )
        return make_response(jsonify(response_object)), 400

@users_blueprint.route('/users/<user_id>', methods=['GET'])
def get_single_user(user_id):
    """Get details of a single user."""
    response_object = dict(
        status='fail',
        message='User does not exist.'
    )
    try:
        user = User.query.filter_by(id=user_id).first()
        if not user:
            return make_response(jsonify(response_object)), 404
        response_object = dict(
            status='success',
            data=dict(
                user_name=user.user_name,
                email=user.email,
                created_at=user.created_at
            )
        )
        return make_response(jsonify(response_object)), 200
    except exc.DataError:
        return make_response(jsonify(response_object)), 404


@users_blueprint.route('/users', methods=['GET'])
def get_all_users():
    """Get all existing users."""
    users = User.query.all()
    users_list = list()
    for user in users:
        user_object = dict(
            id=user.id,
            user_name=user.user_name,
            email=user.email,
            created_at=user.created_at
        )
        users_list.append(user_object)
    response_object = dict(
        status='success',
        data=dict(
            users=users_list
        )
    )
    return make_response(jsonify(response_object)), 200
